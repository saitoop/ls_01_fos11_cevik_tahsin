
public class folgen {
	public static void main(String[] args) {

		foelgenB();
		foelgenC();
		foelgenD();
		foelgenE();
	}

	public static void foelgenB() {

		for (int i = 1; i <= 20; i++) {
			System.out.print(i * i + ",");

		}
		System.out.print("\n");
	}

	public static void foelgenC() {

		for (int i = 2; i <= 102; i = i + 4) {
			System.out.print(i + ",");
		}
		System.out.print("\n");
	}

	public static void foelgenD() {

		for (int i = 2; i <= 32; i = i + 2) {
			System.out.print(i * i + ",");
		}
		System.out.print("\n");
	}

	public static void foelgenE() {

		for (int i = 2; i <= 32768; i = i * 2) {

			System.out.print(i + ",");

		}
	}

}

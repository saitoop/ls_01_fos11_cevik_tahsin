import java.util.Scanner;
public class Fahrzeiten {

	public static void main (String[]args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'j';
		char endetIn = 'h';
		
		fahrzeit =  fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau
		
		System.out.println("Halten in Spandau?");
		haltInSpandau = myScanner.next().charAt(0);
		
		if (haltInSpandau == 'j') {
			
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}
		System.out.println("Richtung Hamburg?");
		richtungHamburg = myScanner.next().charAt(0);
		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96; // Endstation Hamburg
		}
		else {
			fahrzeit = fahrzeit + 34;
			System.out.println("Halt in Stendal?");
			haltInStendal = myScanner.next().charAt(0);
			if (haltInStendal == 'j') {
				fahrzeit = fahrzeit + 16;
			}
			else {
				fahrzeit = fahrzeit + 6; // halt westlich Stendal
			}
			System.out.println("Wo endet die Fahrt?");
			endetIn = myScanner.next().charAt(0);
			if (endetIn == 'w') {
				fahrzeit = fahrzeit + 29;
			}
			else if (endetIn == 'b') {
				fahrzeit = fahrzeit + 21;
			}
			else {
				fahrzeit = fahrzeit +34;
			}
		}
		System.out.println("Die Fahrzeit betr�gt:" + fahrzeit + " minuten");
	}
	
}


public class AusgabenBeispiele {

	public static void main(String[] args) {
	
		// Kommazahl
		
		System.out.printf(" Zahl: *%-20.2f* %n ", 12345.6789);
		
		//Ganzezahl
		
		System.out.printf(" Zahl: *%-20d* %n", 10);
		
		//Zeichenkette
		System.out.printf(" Wort: *%-20s*", "abcdef");
		
	}

}

import java.util.*;

public class Urlaub {
	public static void main (String[]args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben Sie ihr Reisegeld Betrag ein");
		double zahl1 = sc.nextDouble();
		
		double USA = 1.12;
		double JP = 126.50;
		double ENG = 0.89;
		double SCH = 1.08;
		double SCHWE = 10.10;
		
		double ergebnisusa = usarechnung(zahl1, USA);
		System.out.printf("Der Eurobetrag in US-Amerikanischem Geld betr�gt: %.2f USD \n" , ergebnisusa);
		
		double ergebnisjp = jprechnung(zahl1, JP);
		System.out.printf("Der Eurobetrag in Japanischem Geld betr�gt: %.2f Yen \n" , ergebnisjp);
		
		double ergebniseng = engrechnung (zahl1, ENG);
		System.out.printf("Der Eurobetrag in Englischem Geld betr�gt: %.2f GBP \n" , ergebniseng);
		
		double ergebnissch = schrechnung (zahl1, SCH);
		System.out.printf("Der Eurobetrag in Schweizer Geld betr�gt: %.2f CHF \n" , ergebnissch);
		
		
		double ergebnisschwe = schwerechnung (zahl1, SCHWE);
		System.out.printf("Der Eurobetrag in Schwedischen Kronen betr�gt: %.2f SEK \n" , ergebnisschwe);
		
		
	}
	
	public static double usarechnung (double Euro, double USD) {
		
		double ergebnisusa = Euro * USD;
		return ergebnisusa;
	}
	
	public static double jprechnung (double Euro, double Yen) {
		
		double ergebnisjp = Euro * Yen;
		return ergebnisjp;
	}
	public static double engrechnung (double Euro, double GBP) {
		
		double ergebniseng = Euro * GBP;
		return ergebniseng;
	}

	public static double schrechnung (double Euro, double CHF) {
		
		double ergebnissch = Euro * CHF;
		return ergebnissch;
	}
	
	public static double schwerechnung (double Euro, double SEK) {
		
		double ergebnisschwe = Euro * SEK;
		return ergebnisschwe;
	}
}

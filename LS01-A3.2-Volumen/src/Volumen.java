import java.util.*;

public class Volumen {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// W�rfel
		System.out.println("Geben sie eine Zahl f�r den W�rfel ein:");
		double zahl1 = sc.nextDouble();
		double ergebnisw = berrechnew�rfel(zahl1);
		System.out.printf("Das Volumen des W�rfels betr�gt: %.2f\n" , ergebnisw);

		// Quader
		System.out.println("Geben sie die erste Zahl f�r den Quader ein:");
		double zahl2 = sc.nextDouble();
		System.out.println("Geben sie die zweite Zahl f�r den Quader ein:");
		double zahl3 = sc.nextDouble();
		System.out.println("Geben sie die dritte Zahl f�r den Quader ein:");
		double zahl4 = sc.nextDouble();
		double ergebnisq = berrechnequader(zahl2, zahl3, zahl4);
		System.out.printf("Das Volumen des Quaders betr�gt: %.2f\n" , ergebnisq);

		// Pyramide
		System.out.println("Geben sie die erste Zahl f�r die Pyramide ein:");
		double zahl5 = sc.nextDouble();
		System.out.println("Geben sie die zweite Zahl f�r die Pyramide ein:");
		double zahl6 = sc.nextDouble();
		double ergebnisp = berrechnepyramide(zahl5, zahl6);
		System.out.printf("Das Volumen der Pyramide betr�gt: %.2f\n" , ergebnisp);

		// Kugel
		System.out.println("Geben sie die erste Zahl f�r die Kugel ein:");
		double zahl7 = sc.nextDouble();
		double zahl8 = Math.PI;
		double ergebnisk = berrechnekugel(zahl7, zahl8);
		System.out.printf("Das Volumen der Kugel betr�gt: %.2f\n" , ergebnisk);

		sc.close();
	}

	public static double berrechnew�rfel(double seite) {
		double ergebnisw = seite * seite * seite;
		return ergebnisw;
	}
	
	public static double berrechnequader(double h�he, double l�nge, double breite) {
		double ergebnisq = h�he * l�nge * breite;
		return ergebnisq;
		
		
	}
	
	public static double berrechnepyramide(double zahl5, double zahl6) {
	double ergebnisp = zahl5 * zahl5 * zahl6 /3;
	return ergebnisp;
	}
	public static double berrechnekugel(double zahl7, double zahl8) {
		double ergebnisk = 4/3 * zahl7 * zahl7 * zahl7 * zahl8;
		return ergebnisk;
	}
	
}
